package com.example.practico_broadcast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.widget.TextView;

import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.GeofencingClient;
import com.google.android.gms.location.GeofencingRequest;
import com.google.android.gms.location.LocationServices;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private static final String GEOFENCE_ID = "mi_geofence_id";

    private GeofencingClient geofencingClient;
    private List<Geofence> geofenceList;
    private TextView gpsStatusTextView,txt2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gpsStatusTextView = findViewById(R.id.txt1);
        txt2 = findViewById(R.id.txtgeofence);



        geofencingClient = LocationServices.getGeofencingClient(this);
        geofenceList = new ArrayList<>();
        LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        boolean isGpsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        if (isGpsEnabled) {
            gpsStatusTextView.setText("Estado del GPS: Habilitado");
            gpsStatusTextView.setText("hola");
        } else {
            gpsStatusTextView.setText("Estado del GPS: Deshabilitado");
            showGpsDisabledDialog();
        }

        // Define los parámetros del geofence
        double latitude = 37.4219983;
        double longitude = -122.084;
        float radius = 2;
        long expirationDuration = -1;
        int transitionType = Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT;

        // Crea un PendingIntent que se activará cuando se entre o se salga del geofence
        Intent intent = new Intent(this, GeofenceReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Agrega el geofence
        geofenceList.add(new Geofence.Builder()
                .setRequestId(GEOFENCE_ID)
                .setCircularRegion(latitude, longitude, radius)
                .setTransitionTypes(transitionType)
                .setExpirationDuration(expirationDuration)
                .build());


        GeofencingRequest geofencingRequest = new GeofencingRequest.Builder()
                .setInitialTrigger(GeofencingRequest.INITIAL_TRIGGER_ENTER)
                .addGeofences(geofenceList)
                .build();

        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        geofencingClient.addGeofences(geofencingRequest, getGeofencePendingIntent())
                .addOnSuccessListener(this, aVoid -> {
                    txt2.setText("Geofence registrado con exito");
                })
                .addOnFailureListener(this, e -> {
                    txt2.setText("Error al registrar el geopfence");
                });


        LocalBroadcastManager.getInstance(this).registerReceiver(
                new BroadcastReceiver() {
                    @Override
                    public void onReceive(Context context, Intent intent) {
                        if (intent.getAction() != null && intent.getAction().equals("geofence_notification")) {
                            String message = intent.getStringExtra("message");
                            if (message != null) {
                                txt2.setText(message);
                            }
                        }
                    }
                },
                new IntentFilter("geofence_notification")
        );
    }

    private void showGpsDisabledDialog() {

        Intent intent = new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    private PendingIntent getGeofencePendingIntent() {
        Intent intent = new Intent(this, GeofenceReceiver.class);
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT | PendingIntent.FLAG_IMMUTABLE);
    }
}