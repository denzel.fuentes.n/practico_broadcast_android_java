package com.example.practico_broadcast;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import androidx.localbroadcastmanager.content.LocalBroadcastManager;

public class GeofenceReceiver extends BroadcastReceiver {
    private static final String TAG = "GeofenceReceive";
    private  TextView txt ;
    private static GeofenceReceiver instance;
    public static synchronized GeofenceReceiver getInstance() {
        if (instance == null) {
            instance = new GeofenceReceiver();
        }
        return instance;
    }
    @Override
    public void onReceive(Context context, Intent intent) {
        // Aquí maneja las acciones de entrada y salida de geofences
        if (intent.getAction() != null) {
            if (intent.getAction().equals("com.google.android.gms.location.GEOFENCE_TRANSITION_ENTER")) {
                sendNotification(context,"entro al geofence");
                //showToast(context, "Has entrado en una geofence");
            } else if (intent.getAction().equals("com.google.android.gms.location.GEOFENCE_TRANSITION_EXIT")) {
                //showToast(context, "Has salido de una geofence");
                //sendNotification(context,"salio del geofence");
                //txt.setText("Has salido de una geofence");
            }
        }
    }

    private void showToast(Context context, String message) {
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
    }
    private void sendNotification(Context context, String message) {
        // Crea una notificación o actualiza un TextView en MainActivity
        Intent broadcastIntent = new Intent("geofence_notification");
        broadcastIntent.putExtra("message", message);
        LocalBroadcastManager.getInstance(context).sendBroadcast(broadcastIntent);
    }
    public  void setTxt(TextView txt){
        this.txt = txt;
    }
}